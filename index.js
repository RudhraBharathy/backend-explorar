const mysql = require('mysql2/promise');
const fs = require('fs').promises;

const xmlFiles = [
  './XML Dataset/data1.xml',
  './XML Dataset/data2.xml',
  './XML Dataset/data3.xml',
  './XML Dataset/data4.xml',
  './XML Dataset/data5.xml',
];

const connectionConfig = {
  host: 'localhost',
  port: 3306,
  user: 'root',
  password: 'Rud@12345',
  database: 'SIH2023',
};

async function insertXmlContent(xmlFileName, connection) {
  try {
    const xmlContent = await fs.readFile(xmlFileName, 'utf-8');
    const query = 'INSERT INTO SIH2023.xmlfiles (xml_content) VALUES (?)';
    await connection.execute(query, [xmlContent]);
    console.log(`Inserted XML content from ${xmlFileName} into the database.`);
  } catch (error) {
    console.error(`Error inserting XML content from ${xmlFileName}:`, error);
  }
}

async function main() {
  try {
    const connection = await mysql.createConnection(connectionConfig);
    for (const xmlFile of xmlFiles) {
      await insertXmlContent(xmlFile, connection);
    }
    await connection.end();
  } catch (error) {
    console.error('Error connecting to the database:', error);
  }
}
main();
